%SCRIPT
dia = new UniversalInputDialog();
dia.setWindowTitle("Insert Image");
dia.add("images/", "ImagePath", "image");
dia.add(1.0, "TextWidth Multiplier", "widthMultiplier");
dia.add("fig:", "Label", "label");
dia.add("", "Caption", "caption");
dia.add(true, "Center image", "center");


if (dia.exec() != "") {
	var inputStr = "\\begin{figure}[H]\n";
	if(dia.get("center") == true) {
		inputStr += "\t\\begin{center}\n";
	}
	
	inputStr+="\t\t\\includegraphics[width=" + dia.get("widthMultiplier") + "\\textwidth]{"
					+ dia.get("image") + "}\n";
	
	var label = dia.get("label");
	if(label != "") {
		inputStr += "\t\t\\label{" + label + "}\n";
	}
	var caption = dia.get("caption");
	if(caption != "") {
		inputStr += "\t\t\\caption{" + caption + "}\n";
	}
	if(dia.get("center") == true) {
		inputStr += "\t\\end{center}\n";
	}
	
	inputStr += "\\end{figure}\n";
	
	cursor.insertText(inputStr, keepAnchor=false);
}